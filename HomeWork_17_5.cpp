﻿#include <iostream>
#include <cmath>

class Vector
{
private:
    int x = 0;
    int y = 0;
    int z = 0;

public:
    Vector()
    {}
    Vector(int x1, int  y1, int z1) : x(x1), y(y1), z(z1)
    {}

    double length()
    {
        return sqrt(x * x + y * y + z * z);
    }
};

int main()
{
    Vector v(1, 2, 3);
    std::cout << v.length() << '\n';
}
